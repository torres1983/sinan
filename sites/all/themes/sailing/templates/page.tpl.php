<?php
$imagepath=drupal_get_path('theme', 'sailing');
//drupal_add_css($imagepath.'/css/m.css', array('group' => CSS_THEME));
drupal_add_js($imagepath.'/js/jquery.countdown.js', array('scope' => 'footer',));
drupal_add_js($imagepath.'/js/script.js', array('scope' => 'footer',));
?>
<div id="uid" style="display:none;"><?=$user->uid?></div>
<div class="zhezhao"></div>
<!--登录-->
<div class="pop-up log-on">
	<i class="popUp-close"></i>
	<i class="i-ico4"><img src="/<?=$imagepath?>/images/global/icon4.png" /></i>
    <input type="text" placeholder="电子邮箱" class="popUP-input email" />
    <input type="password" placeholder="密码" class="popUP-input password" />
    <a href="javascript:;" class="popUp-a">登录</a>
    <ul class="popUp-ul">
    	<li class="popUp-ul-l clickforget">忘记密码</li>
        <li class="popUp-ul-r clickUser ">新用户</li>
    </ul>
    <div class="popUp-wrong">继续操作前请注册或登录</div>
</div>
<!--登录 end-->
<!--忘记密码-->
<div class="pop-up rem-pass">
	<i class="popUp-close"></i>
	<h1>忘记密码</h1>
    <input type="text" placeholder="输入电子邮箱" class="popUP-input email" />
    <a href="javascript:;" class="popUp-a send">发送</a>
    <a href="javascript:;" class="popUp-a quxiao">取消</a>
    <ul class="popUp-ul">
        <li class="popUp-ul-r clickBefore">返回登录</li>
    </ul>
    <div class="popUp-wrong">邮箱不能为空</div>
</div>
<!--忘记密码 end-->
<!--新用户注册-->
<div class="pop-up user" >
	<i class="popUp-close"></i>
    <!--1-->
    <div class="user1">
        <h1>新用户注册</h1>
        <input type="text" placeholder="电子邮箱" class="popUP-input email" />
        <input type="text" placeholder="创建密码" class="popUP-input password1" />
        <input type="text" placeholder="确认密码" class="popUP-input password2" />
        <a class="popUp-a clickNext">下一步</a>
        <a href="javascript:;" class="popUp-a quxiao">取消</a>
        <ul class="popUp-ul">
            <li class="popUp-ul-l clickforget">忘记密码</li>
            <li class="popUp-ul-r clickYouUser">已有账户</li>
        </ul>
        <div class="popUp-wrong">邮箱不能为空，密码长度不足8位</div>
    </div>
    <!--2-->
    <div class="user2">
        <h1>完善资料</h1>
        <div class="phoneImg"><img src="/<?=$imagepath?>/images/global/touxiang.gif" onclick="$('#fileup').click();" /><p>选取头像</p></div>
        <input type="file" id="fileup" style="display:none">
        <input type="hidden" name="file_base64" id="file_base64">
        <input type="text" placeholder="我的昵称" class="popUP-input name" />
        <input type="text" placeholder="一句话介绍自己" class="popUP-input word" />
        <a href="javascript:;" class="popUp-a clickFinish">完成</a>
        <div class="popUp-wrong">请输入昵称</div>
    </div>
    <!--3-->
    <div class="user3">
        <i class="i-ico4"><img src="/<?=$imagepath?>/images/global/icon4.png" /></i>
        <div class="success"><font>恭喜注册成功</font></div>
        <a href="/" class="popUp-a clickFhui">确认</a>
    </div>
</div>
<!--新用户注册 end-->
<!--修改密码-->
<div class="pop-up change-pass">
	<i class="popUp-close"></i>
	<h1>修改密码</h1>
    <input type="text" placeholder="请输入目前密码" class="popUP-input changepass1" />
    <input type="text" placeholder="请输入新密码" class="popUP-input changepass2" />
    <input type="text" placeholder="请再次输入新密码" class="popUP-input changepass3" />
    <a href="javascript:;" class="popUp-a clickChange">确认</a>
    <div class="popUp-wrong">邮箱不能为空，密码长度不足8位</div>
</div>
<!--修改密码 end-->


<div id="hd" class="f-w f-cb">
  <div class="r1 f-cb">
    <a href="/" class="logo"><span class="i i-ico1"></span></a>
    <ul class="mod20 f-cb">
      <li><a href="javascript:;">最新动态</a></li>
      <li><a href="/<?=drupal_get_path_alias('node/2');?>">我要参赛</a></li>
      <li><a href="javascript:;">我要观赛</a></li>
    </ul>
    <ul class="mod1 f-cb">
      <?php if($user->uid == 0) { ?>
      <li class="login"><a href="javascript:;" id="login">登录</a></li>
      <li class="reg"><a href="javascript:;" id="user">注册</a></li>
      <?php } else { ?>
      <li class="succ" style="display:block;">
        <i><img src="/<?=$imagepath?>/images/global/succ.png" /></i>
        <a href="javascript:;" class="idname">
		  <?php  
		  	$sql = db_query("select field_nickname_value from {field_data_field_nickname} where entity_id = :entity_id", 
		  					array(':entity_id' => $user->uid));
			foreach($sql as $a) { echo $a->field_nickname_value; }				
		  ?>
        </a>
        <div class="mod21">
          <div class="mail"><span class="i i-ico5"></span><a href="javascript:;">(<span>09</span>)</a></div>
          <div class="inout">
            <a href="javascript:;" id="changePass">修改密码</a>
            <a href="/user/logout">退出登陆</a>
          </div>
        </div>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>



<?php print render($page['content']); ?>

<div id="ft" class="f-cb f-w">
	<div class="f-w">
    	<ul class="f-cb mod30">
        	<li><a href="/<?=drupal_get_path_alias('node/9');?>">联系我们</a></li>
            <li><a href="javascript:;">人才招聘</a></li>
        </ul>
    	<div class="mod6">2016司南杯帆船赛<br>All Rights Reserved.京ICP备09020061号</div>
    </div>
</div>
