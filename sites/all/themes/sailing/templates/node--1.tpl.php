<?php
$imagepath=drupal_get_path('theme', 'sailing');
//drupal_add_css($imagepath.'/css/m.css', array('group' => CSS_THEME));
drupal_add_js($imagepath.'/js/jquery.countdown.js', array('scope' => 'footer',));
drupal_add_js($imagepath.'/js/script.js', array('scope' => 'footer',));
?>
<div id="bd" class="f-w">
  <div class="r2">
    <div id="owl-demo" class="owl-carousel">
      <div class="item">
          <img class="lazyOwl" data-src="/<?=$imagepath?>/images/index/kv1.jpg" alt="">
          <div class="banner-txt">惊涛骇浪  乘风起航 —— 2016司南杯帆船赛摄影大赛展映</div>
      </div>
      <div class="item">
          <img class="lazyOwl" data-src="/<?=$imagepath?>/images/index/kv2.jpg" alt="">
          <div class="banner-txt">惊涛骇浪  乘风起航 —— 2016司南杯帆船赛摄影大赛展映</div>
      </div>
      <div class="item">
          <img class="lazyOwl" data-src="/<?=$imagepath?>/images/index/kv3.jpg" alt="">
          <div class="banner-txt">惊涛骇浪  乘风起航 —— 2016司南杯帆船赛摄影大赛展映</div>
      </div>
    </div>
  </div><!--r2-->
  <div class="r3 mod99">
    <div class="timeBox">
      <font>距离赛事开幕还有</font>
      <div id="countdown"></div>
    </div>
  </div><!--倒计时-->
  <div class="clearfix"></div>
  <div class="r4 f-w">
    <ul class="mod2 f-cb mod-lazy">
      <li>
          <a href="javascript:;">
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/b1.jpg" />
              <p>最新动态</p>
          </a>
      </li>
      <li>
          <a href="javascript:;">
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/b2.jpg" />
              <p>2016赛事信息</p>
          </a>
      </li>
      <li>
          <a href="javascript:;">
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/b3.jpg" />
              <p>图片视频</p>
          </a>
      </li>
      <li>
          <a href="javascript:;">
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/b4.jpg" />
              <p>关于司南</p>
          </a>
      </li>
    </ul>
    <ul class="mod2 mod3 f-cb">
      <li>
        <a href="/<?=drupal_get_path_alias('node/2');?>" target="_blank">
          <img src="/<?=$imagepath?>/images/index/b5.jpg" class="pc-dis">
          <img src="/<?=$imagepath?>/images/index/m-b5.jpg" class="mob-dis">
          <p>我要参赛</p>
        </a>
      </li>
      <li>
        <a href="javascript:;">
          <img src="/<?=$imagepath?>/images/index/b6.jpg" class="pc-dis">
          <img src="/<?=$imagepath?>/images/index/m-b6.jpg" class="mob-dis">
          <p>我要观赛</p>
        </a>
      </li>
    </ul>
      <ul class="mod2 mod9 f-cb mod-lazy">
          <li>
              <a href="javascript:;">
                  <img src="/<?=$imagepath?>/images/index/b8.jpg">
                  <p>广告位</p>
              </a>
          </li>
          <li>
              <a href="javascript:;">
                  <img src="/<?=$imagepath?>/images/index/b9.jpg">
                  <p>广告位</p>
              </a>
          </li>
          <li>
              <a href="javascript:;">
                  <img src="/<?=$imagepath?>/images/index/b10.jpg">
                  <p>广告位</p>
              </a>
          </li>
      </ul>
  </div>
  <div class="r5 f-w">
      <ul class="mod4 ul-jus">
          <li>关注我们：</li>
          <li><a href="javascript:void(0);"><span class="i i-ico2"></span></a></li>
          <li><a href="javascript:void(0);"><span class="i i-ico3"></span></a></li>
      </ul>
  </div><!--follow us-->
  <div class="r6 f-w">
      <table cellpadding="0" cellspacing="0" border="0" width="100%" class="mod5 mod-lazy">
          <tr>
              <td align="left">
              <strong>指导单位：</strong>中国帆船帆板协会、海南省文化广电出版体育厅<strong>主办单位：</strong>三亚市人民政府、三沙市人民政府<strong>支持单位：</strong>海南省
海防与口岸办公室、交通部南海救助局三亚基地<strong>承办单位：</strong>三亚市文化广电出版体育局、三亚司南航海推广发展有限公司
              </td>
          <tr align="left">
              <td>
              <span>合作品牌：</span>
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/c1.png"/>
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/c2.png"/>
              <br />
              <span>合作供应商：</span>
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/c11.png"/>
              <br />
              <span>合作媒体：</span>
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/c3.png"/>
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/c4.png"/>
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/c5.png"/>
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/c6.png"/>
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/c7.png"/>
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/c8.png"/>
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/c9.png"/>
              <img src="/<?=$imagepath?>/images/global/load.png" data-original="/<?=$imagepath?>/images/index/c10.png"/>
              </td>
          </tr>
      </table>
  </div><!--companies-->
</div>