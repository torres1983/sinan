jQuery(function(){
	jQuery('#owl-demo').owlCarousel({
		items: 1,
		autoPlay: true,
		lazyLoad: true,
		navigation: true,
		singleItem:true,
		navigationText: ["上一个","下一个"]
	});
	
	jQuery('.mod-lazy img').lazyload({
		skip_invisible: false,
		effect:"fadeIn",
	});
	
	//登录
	jQuery("#login").click(function(){
		jQuery('.popUp-wrong').hide();
		jQuery(".zhezhao").show();
		jQuery(".log-on").show();
	});
	
	//修改密码
	jQuery("#changePass").click(function(){
		jQuery('.popUp-wrong').hide();
		jQuery(".zhezhao").show();
		jQuery(".change-pass").show();
	});
	
	//注册
	jQuery("#user").click(function(){
		jQuery('.popUp-wrong').hide();
		jQuery(".zhezhao").show();
		jQuery(".user").show();
		jQuery(".user .user1").show();
		jQuery(".user .user2").hide();
		jQuery(".user .user3").hide();
	});
	
	//参赛须知
	jQuery("#noticeClick").click(function(){
		jQuery('.popUp-wrong').hide();
		jQuery(".zhezhao").show();
		jQuery(".notice").show();
	});
	
	//我知道了
	jQuery(".notice .popUp-a").click(function(){
		jQuery('.popUp-wrong').hide();
		jQuery(".zhezhao").hide();
		jQuery(".pop-up").hide();
	});
	
	//关闭弹出框
	jQuery(".popUp-close").click(function(){
		jQuery('.popUp-wrong').hide();
		jQuery(".zhezhao").hide();
		jQuery(".pop-up").hide();
	});
	
	//取消
	jQuery(".quxiao").click(function(){
		jQuery('.popUp-wrong').hide();
		jQuery(".zhezhao").hide();
		jQuery(".pop-up").hide();
	});
	
	//忘记密码
	jQuery(".clickforget").click(function(){
		//jQuery(this).parent().parent().hide();
		jQuery('.popUp-wrong').hide();
		jQuery(this).parents(".pop-up").hide();
		jQuery(".rem-pass").show();
	});
	
	//新用户
	jQuery(".clickUser").click(function(){
		jQuery('.popUp-wrong').hide();
		jQuery(this).parent().parent().hide();
		jQuery(".user").show();
		jQuery(".user .user1").show();
	});
	
	//返回登录
	jQuery(".clickBefore").click(function(){
		jQuery('.popUp-wrong').hide();
		jQuery(this).parent().parent().hide();
		jQuery(".log-on").show();
	});
	
	//返回(确认)
	jQuery(".clickFhui").click(function(){
		jQuery('.popUp-wrong').hide();
		jQuery(this).parent().hide();
		jQuery(".user2").show();
	});
	
	
	//已有账户
	jQuery(".clickYouUser").click(function(){
		jQuery('.popUp-wrong').hide();
		jQuery(this).parents(".pop-up").hide();
		jQuery(".log-on").show();
	});
	
	jQuery('.reply1').click(function(){
		jQuery(this).next('.reply-textarea').show();
	});//回复对话框
	
	if(jQuery(window).width<640){
		jQuery('.succ').click(function(){
			jQuery('.mod21').toggle();
		})
	}
	
	 var showMoreNChildren = function (jQuerychildren, n) {
                    //显示某jquery元素下的前n个隐藏的子元素
                    var jQueryhiddenChildren = jQuerychildren.filter(":hidden");
                    var cnt = jQueryhiddenChildren.length;
                    for ( var i = 0; i < n && i < cnt ; i++) {
                        jQueryhiddenChildren.eq(i).show();
                    }
                    return cnt-n;//返回还剩余的隐藏子元素的数量
                }
 
	//对页中现有的class=showMorehandle的元素，在之后添加显示更多条，并绑定点击行为
	jQuery(".showMoreNChildren").each(function () {
		var pagesize = jQuery(this).attr("pagesize") || 10;
		var jQuerychildren = jQuery(this).children();
		if (jQuerychildren.length > pagesize) {
			for (var i = pagesize; i < jQuerychildren.length; i++) {
				jQuerychildren.eq(i).hide();
			}

			jQuery(".more").click(function () {
				if (showMoreNChildren(jQuerychildren, pagesize) <= 0) {
					//如果目标元素已经没有隐藏的子元素了，就隐藏"点击更多的按钮条"
					jQuery(this).hide();
				};
			});
		}
	});
	
	
	
	if(!placeholderSupport()){   // 判断浏览器是否支持 placeholder
		jQuery('[placeholder]').focus(function() {
			var input = jQuery(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			}
		}).blur(function() {
			var input = jQuery(this);
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			}
		}).blur();
	};   
	
	function placeholderSupport() {
		return 'placeholder' in document.createElement('input');
	}

	jQuery('.log-on .popUp-a').click(function(){
		if(jQuery('.log-on .email').val()=='' || jQuery('.log-on .password').val()==''){
			jQuery('.popUp-wrong').html('请输入正确的邮箱及密码');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			return;
		}
		jQuery.ajax({
			url: '/login.php',
			type: 'POST',
			dataType:"json",
			data: {'username' : jQuery('.log-on .email').val(), 'password' : jQuery('.log-on .password').val()},
		})
		.success(function(data) {
			if(data.status=='ok'){
				location.reload();
			} else if(data.status=='error_1'){
				jQuery('.popUp-wrong').html('该用户不存在');
				jQuery('.popUp-wrong').show();
				setTimeout("jQuery('.popUp-wrong').hide()", 3000);
				return;
			} else if(data.status=='error_2'){
				jQuery('.popUp-wrong').html('密码错误');
				jQuery('.popUp-wrong').show();
				setTimeout("jQuery('.popUp-wrong').hide()", 3000);
				return;
			}
		});
	});
	//忘记密码
	jQuery('.rem-pass .send').click(function(){
		if(jQuery('.rem-pass .email').val()==''){
			jQuery('.popUp-wrong').html('请输入正确的邮箱');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			return;
		}
		jQuery.ajax({
			url: '/forget_pass.php',
			type: 'POST',
			dataType:"json",
			data: { 'email' : jQuery('.rem-pass .email').val()},
		})
		.success(function(data) {
			if(data.status=='ok'){
				//jQuery('.popUp-wrong').html('邮件已发送');
				location.reload();
			}else{
				jQuery('.popUp-wrong').html('该用户不存在');
				jQuery('.popUp-wrong').show();
				setTimeout("jQuery('.popUp-wrong').hide()", 3000);
				return;
			}
		});
	});
	//修改密码
	jQuery('.change-pass .clickChange').click(function() { 
		if(jQuery('.change-pass .changepass2').val() == '' || jQuery('.change-pass .changepass3').val() == ''){
			jQuery('.popUp-wrong').html('新密码不能为空');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			return;
		}
		if(jQuery('.change-pass .changepass2').val() != jQuery('.change-pass .changepass3').val()){
			jQuery('.popUp-wrong').html('两次密码输入不一致');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			return;
		}
		jQuery.ajax({
			url: '/change_pass.php',
			type: 'POST',
			dataType:"json",
			data: { 'uid' : jQuery('#uid').text(), 'pass1' : jQuery('.changepass1').val(), 'pass2' : jQuery('.changepass2').val(), 'pass3' : jQuery('.changepass3').val()},
		})
		.success(function(data) {
			if(data.status=='ok'){
				//jQuery('.popUp-wrong').html('邮件已发送');
				location.reload();
			}else{
				jQuery('.popUp-wrong').html('您输入的密码不对');
				jQuery('.popUp-wrong').show();
				setTimeout("jQuery('.popUp-wrong').hide()", 3000);
				return;
			}
		});
	});
	//注册
	jQuery('.user .clickNext').click(function(){
		var pattern = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
		if(jQuery('.user .email').val()=='' || !pattern.test(jQuery('.user .email').val())){
			jQuery('.popUp-wrong').html('请输入正确的邮箱');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			return;
		}
		if(jQuery('.user .password1').val()=='' || jQuery('.user .password2').val()==''){
			jQuery('.popUp-wrong').html('请输入密码');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			return;
		}
		if(jQuery('.user .password1').val() != jQuery('.user .password2').val()){
			jQuery('.popUp-wrong').html('两次密码输入不一致');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			return;
		}
		jQuery.ajax({
			url: '/step1.php',
			type: 'POST',
			dataType:"json",
			data: { 'username' : jQuery('.user .email').val(), 'password1' : jQuery('.user .password1').val() , 'password2' : jQuery('.user .password2').val()},
		})
		.success(function(data) {
			if(data.status=='ok'){
				jQuery('.popUp-wrong').hide();
				jQuery(".user1").hide();
				jQuery(".user2").show();
				setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			}else{
				jQuery('.popUp-wrong').html('该邮箱已注册');
				jQuery('.popUp-wrong').show();
				setTimeout("jQuery('.popUp-wrong').hide()", 3000);
				return;
			}
		});
	});
	jQuery('.user2 .clickFinish').click(function(){
		if(jQuery('.user .name').val()==''){
			jQuery('.popUp-wrong').html('请输入昵称');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			return;
		}
		if(jQuery('.user .word').val()==''){
			jQuery('.popUp-wrong').html('请用一句话介绍自己');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			return;
		}
		jQuery.ajax({
			url: '/step2.php',
			type: 'POST',
			dataType:"json",
			data: { 'username' : jQuery('.user .email').val(), 'password' : jQuery('.user .password1').val() , 'nickname' : jQuery('.user .name').val(), 'summary' : jQuery('.user .word').val(),'file_base64':jQuery('#file_base64').val()},
		})
		.success(function(data) {
			if(data.status=='ok'){
				jQuery('.popUp-wrong').hide();
				jQuery(".user2").hide();
				jQuery(".user3").show();
				setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			}else{
				jQuery('.popUp-wrong').html('该用户已注册');
				jQuery('.popUp-wrong').show();
				setTimeout("jQuery('.popUp-wrong').hide()", 3000);
				return;
			}
		});
	});
	
	jQuery("#fileup").change(function(){
        var v = jQuery(this).val();
        var reader = new FileReader();
        reader.readAsDataURL(this.files[0]);
        reader.onload = function(e){
            //console.log(e.target.result);
            data= e.target.result;
            data = data.replace(/\+/g, "%2B");
    		data = data.replace(/\&/g, "%26");
            jQuery('#file_base64').val(data);
        };
    });
});

function resetpass(uid) { 
	if(jQuery('.resetpassword1').val() == '' || jQuery('resetpassword2').val() == ''){
		jQuery('.popUp-wrong').html('新密码不能为空');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	if(jQuery('.resetpassword1').val() != jQuery('.resetpassword2').val()){
		jQuery('.popUp-wrong').html('两次密码输入不一致');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	jQuery.ajax({
		url: '/change_pass2.php',
		type: 'POST',
		dataType:"json",
		data: {'uid' : uid, 'pass1' : jQuery('.resetpassword1').val(), 'pass2' : jQuery('.resetpassword2').val()},
	})
	.success(function(data) {
		if(data.status=='ok'){
			//jQuery('.popUp-wrong').html('邮件已发送');
			location.href='/';
		}else{
			jQuery('.popUp-wrong').html('您输入的密码不对');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
			return;
		}
	});
}

function dianzan(uid, number) { 
	if(uid == 0) { 
		jQuery('.popUp-wrong').html('请先登陆或注册');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	jQuery.ajax({
		url: '/dian_zan.php',
		type: 'POST',
		dataType:"json",
		data: {'uid' : uid, 'number' : number},
	})
	.success(function(data) {
		if(data.status=='ok'){
			//jQuery('.popUp-wrong').html('邮件已发送');
			location.reload();
		}else{
			//jQuery('.popUp-wrong').html('您输入的密码不对');
//			jQuery('.popUp-wrong').show();
//			return;
		}
	});
}

function quxiaozan(uid, number) { 
	if(uid == 0) { 
		jQuery('.popUp-wrong').html('请先登陆或注册');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	jQuery.ajax({
		url: '/quxiao_zan.php',
		type: 'POST',
		dataType:"json",
		data: {'uid' : uid, 'number' : number},
	})
	.success(function(data) {
		if(data.status=='ok'){
			//jQuery('.popUp-wrong').html('邮件已发送');
			location.reload();
		}else{
			//jQuery('.popUp-wrong').html('您输入的密码不对');
//			jQuery('.popUp-wrong').show();
//			return;
		}
	});
}