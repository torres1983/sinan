<?php 
	header("Content-Type:text/html; charset=utf-8");
	define('DRUPAL_ROOT', getcwd());
	//define('DRUPAL_ROOT', '/xampp/htdocs/jerry');
	require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
	require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
	session_start();
	
	$username = '';
	$password = '';
	if(isset($_POST['username'])) $username = $_POST['username'];
	if(isset($_POST['password'])) $password = $_POST['password'];
	
	$sql = db_query("select * from {users} where name = :name", array(':name' => $username));
	
	if($sql->rowCount() == 0) {
		$arr = array("status" => "error_1");
		echo json_encode($arr);
		return;
	} else { 
		$account = user_load_by_name($username);
		$uid = user_authenticate($username,$password);
		if($uid) { 
			$form_state['uid'] = $uid;
			user_login_submit(array(), $form_state);
			user_login_finalize($form_state);
			$arr = array("uid" => $uid, "status" => "ok");
			echo json_encode($arr);
			return;
		} else { 
			$arr = array("status" => "error_2");
			echo json_encode($arr);
			return;
		}
	}
?>