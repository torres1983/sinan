<?php
/**
 * @file
 * Returns the HTML for a wrapping container around comments.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728230
 */

// Render the comments and form first to see if we need headings.
$comments = render($content['comments']);
$comment_form = render($content['comment_form']);
?>
<section id="comments">
  <?php if ($comment_form): ?>
    <?php print $comment_form; ?>
  <?php endif; ?>
  
  <?php echo $comments; ?>
</section>
