function fleet_registration(userid) { 
	if(userid == 0) { 
		jQuery('.popUp-wrong').html('请先登陆或注册');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	if(jQuery('#boat_name').val()=='' || jQuery('#boat_registry').val()=='' || jQuery('#home_port').val()=='' || jQuery('#boat_style').val()=='' || jQuery('#boat_lenght').val()=='' || jQuery('#irc').val()=='' || jQuery('#company').val()=='' || jQuery('#shipowner_name').val()=='' || jQuery('#contact_name').val()=='' || jQuery('#contact_email').val()=='' || jQuery('#contact_phone').val()=='' || jQuery('#fleet_profile').val()=='') { 
		jQuery('.popUp-wrong').html('请完善信息');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	
	var pattern = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	var phone_ck = /^1[3,5,8]\d{9}$/;
	if(jQuery('#contact_phone').val()=='' || !phone_ck.test(jQuery('#contact_phone').val())){
		jQuery('.popUp-wrong').html('请输入正确的手机号码');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	if(jQuery('#contact_email').val()=='' || !pattern.test(jQuery('#contact_email').val())){
		jQuery('.popUp-wrong').html('请输入正确的邮箱');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	
	jQuery.ajax({
		url: '/sign_up_team.php',
		type: 'POST',
		dataType:"json",
		data: {'uid' : jQuery('#uid').text(), 'boat_name' : jQuery('#boat_name').val(), 'boat_registry' : jQuery('#boat_registry').val(), 'home_port' : jQuery('#home_port').val(), 'boat_style' : jQuery('#boat_style').val(), 'boat_lenght' : jQuery('#boat_lenght').val(), 'irc' : jQuery('#irc').val(), 'company' : jQuery('#company').val(), 'shipowner_name' : jQuery('#shipowner_name').val(), 'contact_name' : jQuery('#contact_name').val(), 'contact_email' : jQuery('#contact_email').val(), 'contact_phone' : jQuery('#contact_phone').val(), 'fleet_profile' : jQuery('#fleet_profile').val()},
	})
	.success(function(data) {
		if(data.status=='ok'){
			location.href='/node/5';
		} else {
			jQuery('.popUp-wrong').html('对不起!你已经报过名了!');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		}
	});
}

function personal_registration(userid) { 
	if(userid == 0) { 
		jQuery('.popUp-wrong').html('请先登陆或注册');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	
	if(jQuery('#name').val()=='' || jQuery('#age').val()=='' || jQuery('#phone').val()=='' || jQuery('#email').val()=='' || jQuery('#irc').val()=='' || jQuery('#person_profile').val()=='' || jQuery('#address').val()=='' || jQuery('#path').val()=='') { 
		jQuery('.popUp-wrong').html('请完善信息');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	
	var pattern = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	var phone_ck = /^1[3,5,8]\d{9}$/;
	if(jQuery('#phone').val()=='' || !phone_ck.test(jQuery('#phone').val())){
		jQuery('.popUp-wrong').html('请输入正确的手机号码');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	if(jQuery('#email').val()=='' || !pattern.test(jQuery('#email').val())){
		jQuery('.popUp-wrong').html('请输入正确的邮箱');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	var sex = getRadioValue();
	
	jQuery.ajax({
		url: '/sign_up_person.php',
		type: 'POST',
		dataType:"json",
		data: {'uid' : jQuery('#uid').text(), 'name' : jQuery('#name').val(), 'sex' : sex, 'age' : jQuery('#age').val(), 'phone' : jQuery('#phone').val(), 'email' : jQuery('#email').val(), 'irc' : jQuery('#irc').val(), 'person_profile' : jQuery('#person_profile').val(), 'address' : jQuery('#address').val(), 'path' : jQuery('#path').val()},
	})
	.success(function(data) {
		if(data.status=='ok'){
			location.href='/node/5';
		} else {
			jQuery('.popUp-wrong').html('对不起!你已经报过名了!');
			jQuery('.popUp-wrong').show();
			setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		}
	});
}

function personal_change() {
	if(jQuery('#name').val()=='' || jQuery('#age').val()=='' || jQuery('#phone').val()=='' || jQuery('#email').val()=='' || jQuery('#irc').val()=='' || jQuery('#person_profile').val()=='' || jQuery('#address').val()=='' || jQuery('#path').val()=='') { 
		jQuery('.popUp-wrong').html('请完善信息');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	
	var pattern = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	var phone_ck = /^1[3,5,8]\d{9}$/;
	if(jQuery('#phone').val()=='' || !phone_ck.test(jQuery('#phone').val())){
		jQuery('.popUp-wrong').html('请输入正确的手机号码');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	if(jQuery('#email').val()=='' || !pattern.test(jQuery('#email').val())){
		jQuery('.popUp-wrong').html('请输入正确的邮箱');
		jQuery('.popUp-wrong').show();
		setTimeout("jQuery('.popUp-wrong').hide()", 3000);
		return;
	}
	
	var sex = getRadioValue();
	jQuery.ajax({
		url: '/person_change.php',
		type: 'POST',
		dataType:"json",
		data: {'uid' : jQuery('#uid').text(), 'name' : jQuery('#name').val(), 'sex' : sex, 'age' : jQuery('#age').val(), 'phone' : jQuery('#phone').val(), 'email' : jQuery('#email').val(), 'irc' : jQuery('#irc').val(), 'person_profile' : jQuery('#person_profile').val(), 'address' : jQuery('#address').val(), 'path' : jQuery('#path').val()},
	})
	.success(function(data) {
		if(data.status=='ok'){
			location.href='/node/7?number='+jQuery('#uid').text();
		} else {
			//jQuery('.popUp-wrong').html('此船名已被使用');
			//jQuery('.popUp-wrong').show();
		}
	});
}


function getRadioValue() {
    for(i=0;i<document.getElementsByName("radiobutton").length;i++) {
		if(document.getElementsByName("radiobutton")[i].checked) { 
			return (document.getElementsByName("radiobutton")[i].value);
		}
    }
}