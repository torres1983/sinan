<?php 
	header("Content-Type:text/html; charset=utf-8");
	define('DRUPAL_ROOT', getcwd());
	//define('DRUPAL_ROOT', '/xampp/htdocs/jerry');
	require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
	require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
	session_start();
	
	$username = '';
	$password1 = '';
	$password2 = '';
	if(isset($_POST['username'])) $username = $_POST['username'];
	if(isset($_POST['password1'])) $password1 = (filter_xss($_POST['password1']));
	if(isset($_POST['password2'])) $password2 = (filter_xss($_POST['password2']));
	
	$sql = db_query("select * from {users} where mail = :mail", array(':mail' => $username));
	if($sql->rowCount() != 0) { 
		$arr = array("status" => "error");
	} else { 
		$arr = array("status" => "ok");
	}
	
	echo json_encode($arr);
?>