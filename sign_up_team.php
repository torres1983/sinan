<?php 
	header("Content-Type:text/html; charset=utf-8");
	define('DRUPAL_ROOT', getcwd());
	//define('DRUPAL_ROOT', '/xampp/htdocs/jerry');
	require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
	require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
	session_start();
	
	$uid = '';
	$boat_name = '';
	$boat_registry = '';
	$home_port = '';
	$boat_style = '';
	$boat_lenght = '';
	$irc = '';
	$company = '';
	$shipowner_name = '';
	$contact_name = '';
	$contact_email = '';
	$contact_phone = '';
	$fleet_profile = '';
	if(isset($_POST['uid'])) $uid = $_POST['uid'];
	if(isset($_POST['boat_name'])) $boat_name = $_POST['boat_name'];
	if(isset($_POST['boat_registry'])) $boat_registry = $_POST['boat_registry'];
	if(isset($_POST['home_port'])) $home_port = $_POST['home_port'];
	if(isset($_POST['boat_style'])) $boat_style = $_POST['boat_style'];
	if(isset($_POST['boat_lenght'])) $boat_lenght = $_POST['boat_lenght'];
	if(isset($_POST['irc'])) $irc = $_POST['irc'];
	if(isset($_POST['company'])) $company = $_POST['company'];
	if(isset($_POST['shipowner_name'])) $shipowner_name = $_POST['shipowner_name'];
	if(isset($_POST['contact_name'])) $contact_name = $_POST['contact_name'];
	if(isset($_POST['contact_email'])) $contact_email = $_POST['contact_email'];
	if(isset($_POST['contact_phone'])) $contact_phone = $_POST['contact_phone'];
	if(isset($_POST['fleet_profile'])) $fleet_profile = $_POST['fleet_profile'];
	
	$sql = db_query("select * from {boat_team_information} where uid = :uid", array(':uid' => $uid));
	if($sql->rowCount() == 0) { 
		$query = db_insert('boat_team_information') 
		  ->fields(array('uid', 'boat_name', 'boat_registry', 'home_port', 'boat_lenght', 'boat_style', 'irc', 'company', 'shipowner_name', 'contact_name', 'contact_email', 'contact_phone', 'fleet_profile'))
		  ->values(array('uid' => $uid, 'boat_name' => $boat_name, 'boat_registry' => $boat_registry, 'home_port' => $home_port, 'boat_style' => $boat_style, 'boat_lenght' => $boat_lenght, 'irc' => $irc, 'company' => $company, 'shipowner_name' => $shipowner_name, 'contact_name' => $contact_name, 'contact_email' => $contact_email, 'contact_phone' => $contact_phone, 'fleet_profile' => $fleet_profile,))
		->execute();
		
		$arr = array('status'=>'ok');
		echo json_encode($arr);
	} else { 
		$arr = array('status'=>'error');
		echo json_encode($arr);
	}
?>