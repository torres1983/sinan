<?php 
	header("Content-Type:text/html; charset=utf-8");
	define('DRUPAL_ROOT', getcwd());
	//define('DRUPAL_ROOT', '/xampp/htdocs/jerry');
	require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
	require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
	session_start();
	
	$type = '';
	if(isset($_POST['type'])) $type = $_POST['type'];
	
	switch($type) { 
		case 'one':
			comment_one();
			break;
		case 'two':
			comment_two();
			break;
	}
	
	
	function comment_one() { 
		$obj = '';
		$uid = '';
		$shijian = '';
		$content = '';
		$status = 0;
		if(isset($_POST['obj'])) $obj = $_POST['obj'];
		if(isset($_POST['uid'])) $uid = $_POST['uid'];
		if(isset($_POST['shijian'])) $shijian = $_POST['shijian'];
		if(isset($_POST['content'])) $content = $_POST['content'];
		
		$query = db_insert('comment_one') 
		  ->fields(array('cid', 'comment_object', 'comment_uid', 'content', 'status'))
		  ->values(array('cid' => $shijian, 'comment_object' => $obj, 'comment_uid' => $uid, 'content' => $content, 'status' => $status))
		->execute();
		
		$arr = array('status'=>'ok');
		echo json_encode($arr);
	}
	
	function comment_two() { 
		$obj = '';
		$uid = '';
		$content = '';
		//$status = 0;
		if(isset($_POST['obj'])) $obj = $_POST['obj'];
		if(isset($_POST['uid'])) $uid = $_POST['uid'];
		if(isset($_POST['content'])) $content = $_POST['content'];
		
		$query = db_insert('comment_two') 
		  ->fields(array('cid', 'comment_uid', 'content'))
		  ->values(array('cid' => $obj, 'comment_uid' => $uid, 'content' => $content))
		->execute();
		
		$arr = array('status'=>'ok');
		echo json_encode($arr);
	}
?>