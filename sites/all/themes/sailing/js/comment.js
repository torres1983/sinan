// JavaScript Document
function comment_add(obj, uid, shijian) { 
	if(uid == 0) { 
		jQuery('.popUp-wrong').html('请先登陆或注册');
		jQuery('.popUp-wrong').show();
		return;
	}
	if(jQuery('#content').val() == '') { 
		return;
	}	
	jQuery.ajax({
		url: '/comment.php',
		type: 'POST',
		dataType:"json",
		data: {'type' : 'one', 'obj' : obj, 'uid' : uid, 'shijian' : shijian, 'content' : jQuery('#content').val()},
	})
	.success(function(data) {
		if(data.status=='ok'){
			location.reload();
		} else {
			//jQuery('.popUp-wrong').html('此船名已被使用');
			//jQuery('.popUp-wrong').show();
		}
	});
}

function comment_add2(obj, uid) { 
	if(uid == 0) { 
		jQuery('.popUp-wrong').html('请先登陆或注册');
		jQuery('.popUp-wrong').show();
		return;
	}
	if(jQuery('#content2').val() == '') { 
		return;
	}	
	jQuery.ajax({
		url: '/comment.php',
		type: 'POST',
		dataType:"json",
		data: {'type' : 'two', 'obj' : obj, 'uid' : uid, 'content' : jQuery('#content2').val()},
	})
	.success(function(data) {
		if(data.status=='ok'){
			location.reload();
		} else {
			//jQuery('.popUp-wrong').html('此船名已被使用');
			//jQuery('.popUp-wrong').show();
		}
	});
}