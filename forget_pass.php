<?php 
	header("Content-Type:text/html; charset=utf-8");
	define('DRUPAL_ROOT', getcwd());
	//define('DRUPAL_ROOT', '/xampp/htdocs/jerry');
	require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
	require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
	session_start();
	
	$email = '';
	if(isset($_POST['email'])) $email = $_POST['email'];
	
	$account = user_load_by_name($email);
	if($account){
		$temp = user_pass_rehash($account->pass, $timestamp, $account->login);
		$url = 'http://'.$_SERVER['SERVER_NAME'].'/reset-password?uid='.$account->uid.'&temp='.$temp;
		$query = db_insert('get_forget_pass') 
		  ->fields(array('uid', 'temp'))
		  ->values(array('uid' => $account->uid, 'temp' => $temp,))
		->execute();
		
		$sendfrom = 'admin@sydney.cn';
		ini_set('sendmail_from', $sendfrom);
		sendmail_forgetpwd($email,$sendfrom,$url,$account);		
	} else {
		$arr = array('status'=>'error');
		print json_encode($arr);
	}
	
	function sendmail_forgetpwd($to,$from,$url,$account){	
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= "To: $to \r\n"; 
		$headers .= "From: $from \r\n" .
	    "Reply-To: $from" . "\r\n" .
	    'X-Mailer: PHP/' . phpversion() ." X-SENDER:$from";
	    $subject = "Destination NSW Website Account Password Reset. 新南威尔士州旅游局旅游网站－重设密码";  
		$subject = "=?UTF-8?B?".base64_encode($subject)."?=";
	 
	    $content = 'Dear '.$account->field_first_name['und'][0]['value'].' '.$account->field_last_name['und'][0]['value'].',';
		$content .= '<br /><br />';
	    $content .= 'We received a request to reset the password for your account.'.'<br />';
		$content .= 'To reset your password, please click on the link below.'.'<br /><br />';
		$content .= 'Regards,<br />';
		$content .= 'the Destination NSW Team.';
		$content .= '<br /><br />';
		$content .= '您好：'.'<br /><br />';
		$content .= '我们收到您重设帐户密码的申请。'.'<br />';
		$content .= '請点击以下链接重设密碼。'.'<br /><br />';
		$content .= '新南威尔士州旅游局';
		$content .= '<br /><br />';
		$content .= '你好：'.'<br /><br />';
		$content .= '我們收到你重設帳號密碼的申請。'.'<br />';
		$content .= '請點擊以下鏈結重設密碼。'.'<br /><br />';
		$content .= '新南威爾士州旅遊局';
		$content .= '<br /><br />';
		
		$content .= '<a href="'.$url.'" target="_blank">'.$url.'</a><br />';
		
	    $result = mail($to, $subject, $content, $headers);  
		
		if($result)
		{
			$arr=array('status'=>'ok');
			echo json_encode($arr);
		}else{
			echo '1';
		}
	}
?>