<?php
$imagepath=drupal_get_path('theme', 'sailing');
drupal_add_css($imagepath.'/css/e.css', array('group' => CSS_THEME));
drupal_add_js($imagepath.'/js/sign_up.js', array('scope' => 'footer',));
?>
<div id="bd" class="f-w">
  <div class="mod7"><p><?=$title?></p></div>
  <div class="mod100">
    <img src="/<?=$imagepath?>/images/apply/banner.jpg" />
    <ul>
      <?php $sql = db_query("select * from {personal_information} where uid = :uid", array(':uid' => $user->uid)); ?>
      <?php foreach($sql as $a) { ?>
      <li><span>姓名：</span><input type="text" id="name" value="<?=$a->name?>" /></li>
      <li>
        <span>性别：</span>
        <label><input type="radio" name="radiobutton" value="nan" <?php if($a->sex == 'nan') echo 'checked'; ?> class="radio"> 
          <font>男</font></label>
        <label><input type="radio" name="radiobutton" value="nv" <?php if($a->sex == 'nv') echo 'checked'; ?> class="radio"> 
          <font>女</font></label>
      </li>
      <li><span>年龄：</span><input type="text" id="age" value="<?=$a->age?>" /></li>
      <li><span>手机：</span><input type="text" placeholder="您的手机信息将被保密，不会公开" id="phone" value="<?=$a->phone?>" /></li>
      <li><span>邮箱：</span><input type="text" placeholder="您的邮箱信息将被保密，不会公开" id="email" value="<?=$a->email?>" /></li>
      <li><span>IRC操作位置：</span><input type="text" id="irc" value="<?=$a->irc?>" /></li>
      <li>
        <span>个人履历：</span>
        <textarea placeholder="不少于一百字" id="person_profile"><?=$a->person_profile?></textarea>
      </li>
      <li><span>居住地：</span><input type="text" id="address" value="<?=$a->address?>" /></li>
      <!--<li><span>照片：</span>
        <input type="text" name="upfile" id="upfile" class="file-text">  
        <input type="button" value="本地上传" onclick="path.click()" class="file-butt">  
        <input type="file" id="path" style="display:none" onchange="upfile.value=this.value">
      </li>-->
      <?php } ?>
    </ul>
    <a class="mod8" target="_parent" onclick="personal_change();">提交</a>    
  </div>
  <div class="popUp-wrong">邮箱不能为空</div>
  <div class="clearfix"></div>
</div>