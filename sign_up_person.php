<?php 
	header("Content-Type:text/html; charset=utf-8");
	define('DRUPAL_ROOT', getcwd());
	//define('DRUPAL_ROOT', '/xampp/htdocs/jerry');
	require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
	require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
	drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
	drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
	session_start();
	
	$uid = '';
	$name = '';
	$sex = '';
	$age = '';
	$phone = '';
	$email = '';
	$irc = '';
	$person_profile = '';
	$address = '';
	$path = '';

	if(isset($_POST['uid'])) $uid = $_POST['uid'];
	if(isset($_POST['name'])) $name = $_POST['name'];
	if(isset($_POST['sex'])) $sex = $_POST['sex'];
	if(isset($_POST['age'])) $age = $_POST['age'];
	if(isset($_POST['phone'])) $phone = $_POST['phone'];
	if(isset($_POST['email'])) $email = $_POST['email'];
	if(isset($_POST['irc'])) $irc = $_POST['irc'];
	if(isset($_POST['person_profile'])) $person_profile = $_POST['person_profile'];
	if(isset($_POST['address'])) $address = $_POST['address'];
	//if(isset($_POST['path'])) $path = $_POST['path'];
	$path = 0;
	
	$sql = db_query("select * from {personal_information} where uid = :uid", array(':uid' => $uid));
	
	if($sql->rowCount() == 0) {
		$query = db_insert('personal_information') 
		  ->fields(array('uid', 'name', 'sex', 'age', 'phone', 'email', 'irc', 'person_profile', 'address', 'path'))
		  ->values(array('uid' => $uid, 'name' => $name, 'sex' => $sex, 'age' => $age, 'phone' => $phone, 'email' => $email, 'irc' => $irc, 'person_profile' => $person_profile, 'address' => $address, 'path' => $path))
		->execute();
		
		$arr = array('status'=>'ok');
	} else { 
		$arr = array('status'=>'error');
	}
	echo json_encode($arr);
?>