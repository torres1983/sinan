<?php
$imagepath=drupal_get_path('theme', 'sailing');
drupal_add_css($imagepath.'/css/m-page.css', array('group' => CSS_THEME));
drupal_add_js($imagepath.'/js/comment.js', array('scope' => 'footer',));
if(isset($_GET['number'])) $number = $_GET['number'];
$sql = db_query("select * from {personal_information} where uid = :uid", array(':uid' => $number));
$zan = db_query("select * from {an_zan} where bei_an_uid = :uid", array(':uid' => $number));
//echo $zan->rowCount();
?>
<div id="bd" class="f-w">
  <div class="r10">
    <div class="mod7"><p><?=$title?></p></div>
    <div class="mod11">
      <ul class="mod12 f-cb">
      <?php foreach($sql as $a) { ?>
      <li><img src="/<?=$imagepath?>/images/apply/number.jpg" /></li>
      <li><label>姓名：</label><p><?=$a->name?></p></li>
      <li><label>性别:</label><p><?php if($a->sex == 'nan') { echo '男'; } else { echo '女'; }?></p></li>      
      <li><label>年龄：</label><p><?=$a->age?></p></li>
      <li><label>居住地：</label><p><?=$a->address?></p></li>
      <li><label>擅长位置:</label><p><?=$a->irc?></p></li>
      <?php if($number == $user->uid) { ?>
      <li><label>手机:</label><p><?=$a->phone?></p></li>
      <li><label>邮箱:</label><p><?=$a->email?></p></li>
      <?php } ?>
      <li class="personal"><label>个人履历:</label>
        <p><?=$a->person_profile?></p>
      </li>
      <?php 
	  	$sqla = db_query("select * from {an_zan} where dian_uid = :uid and bei_an_uid = :number", 
	  					array(':uid' => $user->uid, ':number' => $number));
		if($sqla->rowCount() == 0) {
	  ?>
      <li><a onclick="dianzan('<?=$user->uid?>','<?=$number?>');">赞<span class="i i-ico16"></span>(<span><?=$zan->rowCount()?></span>)</a></li>
      <? } else { ?>
      <li><a onclick="quxiaozan('<?=$user->uid?>','<?=$number?>');">取消<span class="i i-ico16"></span>(<span><?=$zan->rowCount()?></span>)</a></li>
      <?php } ?>
      <?php if($user->uid == $number) { ?>
      <li><a href="/<?=drupal_get_path_alias('node/8');?>" class="mod8 mod16">修改个人信息</a></li>
      <?php } ?>
      
      <?php } ?>
      </ul>
      <ul class="mod13 f-cb">
        <?php 
	      $sql = db_query("select * from {comment_one} where comment_object = :obj and status = :status order by cid desc", 
		  				  array(':obj' => $number, ':status' => 1));
		  foreach($sql as $a) { 
		  	$commentator = user_load($a->comment_uid);
			$shijian = explode(" ",date('Y-m-d h:i:s', $a->cid));
	    ?>
        <li>
          <div class="info"><span><?=$shijian[0]?></span><span><?=$shijian[1]?></span><span><?=$commentator->name?></span></div>
          <p class="txt"><?=$a->content?></p>
          <?php $sql2 = db_query("select * from {comment_two} where cid = :cid order by cid desc", array(':cid' => $a->cid)); ?>
          <?php if($sql2->rowCount() != 0) { ?>
          <?php foreach($sql2 as $b) { ?>
          <div class="reply">回复<span><?=$commentator->name?></span>：<?=$b->content?></div>
          <?php } } ?>
          <a href="javascript:;" class="a-reply reply1">回复</a>
          <div class="reply-textarea">
              <textarea id="content2"></textarea>
              <a class="a-reply" onclick="comment_add2('<?=$a->cid?>','<?=$user->uid?>');">回复TA</a>
          </div>
        </li>
        <?php } ?>
        <li class="yours">
            <p>我的留言......</p>
            <textarea id="content"></textarea>
            <a class="a-reply" onclick="comment_add('<?=$number?>','<?=$user->uid?>','<?=time()?>');">加上去</a>
        </li>
      </ul>
      <a href="/<?=drupal_get_path_alias('node/6');?>" class="mod8" target="_parent">返回</a>
      <div class="clearfix">&nbsp;</div>
    </div>
  </div>
  <div class="popUp-wrong">邮箱不能为空</div>
</div>
